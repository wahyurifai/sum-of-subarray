import java.util.ArrayList;
import java.util.Scanner;

public class SumOfSubarray {
	private static Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) {
		int total = 0;
		int inArrLengthNew;
		
		System.out.println("<< Sum of Subarray >>");
        System.out.print("Provide Array Length :" );
        String inArrLength = scanner.nextLine();
        inArrLengthNew = Integer.valueOf(inArrLength);
		ArrayList<Integer> listOfArray = new ArrayList<Integer>();
		
		for (int z=0; z<inArrLengthNew; z++){
	        System.out.print("Array "+(z+1)+" :");
	        String objArray = scanner.nextLine();
			listOfArray.add(Integer.valueOf(objArray));
		}
		
		for (int i=0; i<listOfArray.size(); i++) {
			ArrayList<Integer> listArrTemp = new ArrayList<Integer>();
			for (int j=0; j<listOfArray.size(); j++) {
				if (j==i) ; //do nothing
				else {
					listArrTemp.add(listOfArray.get(j));
				}
			}
			if (!listArrTemp.isEmpty()) {
				System.out.println(listArrTemp);
				for (int element : listArrTemp) {
					total += element;
				}
			}
		}
		System.out.println("=========================");
		System.out.println("List of Input Array :"+listOfArray);
		System.out.println("Sum of Total List Subarray : "+total);
		System.exit(0);
	}
}
